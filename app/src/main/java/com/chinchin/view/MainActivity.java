package com.chinchin.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.chinchin.R;
import com.chinchin.model.QR;
import com.chinchin.model.Response;
import com.chinchin.viewModel.PriceViewModel;

public class MainActivity extends AppCompatActivity {
    private PriceViewModel priceViewModel;
    private Spinner fromSpinner;
    private Button buttonCalculate;
    private EditText amount;
    private TextView rateBTC;
    private TextView rateETH;
    private TextView rateUSD;
    private TextView rateEUR;
    private TextView ratePTR;
    private TextView rateBS;
    private TextView valueBTC;
    private TextView valueETH;
    private TextView valueUSD;
    private TextView valueEUR;
    private TextView valuePTR;
    private TextView valueBS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void init() {
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        buttonCalculate = findViewById(R.id.buttonCalculate);
        fromSpinner = findViewById(R.id.from_spinner);
        amount = findViewById(R.id.editTextTextPersonName);
        rateBTC = findViewById(R.id.ratebtc);
        rateETH = findViewById(R.id.rateeth);
        rateUSD = findViewById(R.id.rateusd);
        rateEUR = findViewById(R.id.rateeur);
        ratePTR = findViewById(R.id.rateptr);
        rateBS = findViewById(R.id.ratebs);
        valueBTC = findViewById(R.id.valuebtc);
        valueETH = findViewById(R.id.valueeth);
        valueUSD = findViewById(R.id.valueusd);
        valueEUR = findViewById(R.id.valueeur);
        valuePTR = findViewById(R.id.valueptr);
        valueBS = findViewById(R.id.valuebs);
        initSpinner();
        initPriceViewModel();
        initButtonCalculate();
    }

    private void initSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.currency_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fromSpinner.setAdapter(adapter);
    }

    private void initPriceViewModel() {
        priceViewModel = ViewModelProviders.of(this).get(PriceViewModel.class);
        priceViewModel.getETH().observe(this, new Observer<Response>() {
            @Override
            public void onChanged(Response newResponse) {
                getValues(newResponse);
            }
        });
    }

    private void initButtonCalculate() {
        buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fromSpinner.getSelectedItem().toString() != "PTR" || fromSpinner.getSelectedItem().toString() != "BS") {
                    priceViewModel.init(fromSpinner.getSelectedItem().toString());
                    priceViewModel.getETH().observe(MainActivity.this, new Observer<Response>() {
                        @Override
                        public void onChanged(Response newResponse) {
                            getValues(newResponse);
                        }
                    });
                }
            }
        });
    }

    private void getValues(Response newResponse) {
        QR qr = priceViewModel.calculate(newResponse, amount.getText().toString(), fromSpinner.getSelectedItem().toString());
        valueBS.setText(qr.getValueBS());
        valueBTC.setText(qr.getValueBTC());
        valueETH.setText(qr.getValueETH());
        valueUSD.setText(qr.getValueUSD());
        valuePTR.setText(qr.getValuePTR());
        valueEUR.setText(qr.getValueEuro());
        rateBS.setText(qr.getRateBS());
        rateBTC.setText(qr.getRateBTC());
        rateETH.setText(qr.getRateETH());
        rateUSD.setText(qr.getRateUSD());
        ratePTR.setText(qr.getRatePTR());
        rateEUR.setText(qr.getRateEuro());
    }
}