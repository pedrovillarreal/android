package com.chinchin.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.chinchin.R;

public class LoginActivity extends AppCompatActivity {
    private Button loginButton;
    private EditText editTextTextPersonName;
    private EditText editTextTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    private void init() {
        loginButton = findViewById(R.id.button);
        editTextTextPersonName = findViewById(R.id.editTextTextPersonName);
        editTextTextPassword = findViewById(R.id.editTextTextPassword);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initMainActivity();
            }
        });
    }

    private void initMainActivity() {
        saveUserPreferences(editTextTextPersonName.getText().toString(),
                editTextTextPassword.getText().toString(), "TOKENTEST");
        Intent main = new Intent(this, MainActivity.class);
        startActivity(main);
    }

    private void saveUserPreferences(String username, String password, String token) {
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("username", username);
        editor.putString("password", password);
        editor.putString("token", token);
        editor.commit();
    }
}