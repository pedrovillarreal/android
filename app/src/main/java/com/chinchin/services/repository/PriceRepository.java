package com.chinchin.services.repository;

import androidx.lifecycle.MutableLiveData;

import com.chinchin.model.Response;
import com.chinchin.services.RetrofitRequest;
import com.chinchin.services.webServices.ApiRequest;

import retrofit2.Call;
import retrofit2.Callback;

public class PriceRepository {
    private static PriceRepository priceRepository;

    public static PriceRepository getInstance(){
        if (priceRepository == null){
            priceRepository = new PriceRepository();
        }
        return priceRepository;
    }

    private ApiRequest apiRequest;

    public PriceRepository(){
        apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    }

    public MutableLiveData<Response> getPrice(String currency){
        final MutableLiveData<Response> newsData = new MutableLiveData<>();
        apiRequest.getExchangeRates(currency).enqueue(new Callback<Response>() {

            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                System.out.println(response.body());
                if (response.body() != null) {
                    newsData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                System.out.println("error" + t);
            }
        });
        return newsData;
    }
}
