package com.chinchin.services.webServices;

import com.chinchin.model.Response;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiRequest {
    @GET("exchange-rates")
    Call<Response> getExchangeRates(@Query("currency") String currency);
}