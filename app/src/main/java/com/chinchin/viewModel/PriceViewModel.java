package com.chinchin.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.chinchin.model.QR;
import com.chinchin.model.Response;
import com.chinchin.services.repository.PriceRepository;

public class PriceViewModel extends ViewModel {
    private final String valuePTR = "60";
    private final String valueBS = "100000";
    private MutableLiveData<Response> res = new MutableLiveData<Response>();
    private PriceRepository newsRepository;

    public PriceViewModel() {
        super();
        newsRepository = PriceRepository.getInstance();
        res = newsRepository.getPrice("BTC");
    }

    public void init(String spinner){
        newsRepository = PriceRepository.getInstance();
        res = newsRepository.getPrice(spinner);
    }

    public void select(Response response) {
        res.setValue(response);
    }

    public LiveData<Response> getETH() {
        return res;
    }

    public QR calculate(Response response, String amount, String spinner) {
        QR qr = new QR();

        qr.setAmount(amount);
        qr.setBase(spinner);
        Double rateBS = Double.parseDouble(response.getData().getRates().getUSD()) * Double.parseDouble(amount) * Double.parseDouble(valueBS);
        qr.setValueBS(String.format("%.4f",rateBS));
        qr.setRateBS(valueBS);
        Double ratePTR = Double.parseDouble(response.getData().getRates().getUSD()) * Double.parseDouble(amount) * Double.parseDouble(valuePTR);
        qr.setValuePTR(String.format("%.4f",ratePTR));
        qr.setRatePTR(valuePTR);
        Double rateBTC = Double.parseDouble(response.getData().getRates().getBTC()) * Double.parseDouble(amount);
        qr.setValueBTC(String.format("%.4f",rateBTC));
        qr.setRateBTC(response.getData().getRates().getBTC());
        Double rateETH = Double.parseDouble(response.getData().getRates().getETH()) * Double.parseDouble(amount);
        qr.setValueETH(String.format("%.4f",rateETH));
        qr.setRateETH(response.getData().getRates().getETH());
        Double rateUSD = Double.parseDouble(response.getData().getRates().getUSD()) * Double.parseDouble(amount);
        qr.setValueUSD(String.format("%.4f",rateUSD));
        qr.setRateUSD(response.getData().getRates().getUSD());
        Double rateEUR = Double.parseDouble(response.getData().getRates().getEUR()) * Double.parseDouble(amount);
        qr.setValueEuro(String.format("%.4f",rateEUR));
        qr.setRateEuro(response.getData().getRates().getEUR());

        return qr;
    }

}
