package com.chinchin.model;

public class QR {
    private String amount;
    private String base;
    private String rateETH;
    private String rateBTC;
    private String ratePTR;
    private String rateBS;
    private String rateEuro;
    private String rateUSD;
    private String valueETH;
    private String valueBTC;
    private String valuePTR;
    private String valueBS;
    private String valueEuro;
    private String valueUSD;

    public QR() {}

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getRateETH() {
        return rateETH;
    }

    public void setRateETH(String rateETH) {
        this.rateETH = rateETH;
    }

    public String getRateBTC() {
        return rateBTC;
    }

    public void setRateBTC(String rateBTC) {
        this.rateBTC = rateBTC;
    }

    public String getRatePTR() {
        return ratePTR;
    }

    public void setRatePTR(String ratePTR) {
        this.ratePTR = ratePTR;
    }

    public String getRateBS() {
        return rateBS;
    }

    public void setRateBS(String rateBS) {
        this.rateBS = rateBS;
    }

    public String getRateEuro() {
        return rateEuro;
    }

    public void setRateEuro(String rateEuro) {
        this.rateEuro = rateEuro;
    }

    public String getRateUSD() {
        return rateUSD;
    }

    public void setRateUSD(String rateUSD) {
        this.rateUSD = rateUSD;
    }

    public String getValueETH() {
        return valueETH;
    }

    public void setValueETH(String valueETH) {
        this.valueETH = valueETH;
    }

    public String getValueBTC() {
        return valueBTC;
    }

    public void setValueBTC(String valueBTC) {
        this.valueBTC = valueBTC;
    }

    public String getValuePTR() {
        return valuePTR;
    }

    public void setValuePTR(String valuePTR) {
        this.valuePTR = valuePTR;
    }

    public String getValueBS() {
        return valueBS;
    }

    public void setValueBS(String valueBS) {
        this.valueBS = valueBS;
    }

    public String getValueEuro() {
        return valueEuro;
    }

    public void setValueEuro(String valueEuro) {
        this.valueEuro = valueEuro;
    }

    public String getValueUSD() {
        return valueUSD;
    }

    public void setValueUSD(String valueUSD) {
        this.valueUSD = valueUSD;
    }
}
