package com.chinchin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rates {
    @SerializedName("BTC")
    @Expose
    private String BTC;
    @SerializedName("ETH")
    @Expose
    private String ETH;
    @SerializedName("USD")
    @Expose
    private String USD;
    @SerializedName("EUR")
    @Expose
    private String EUR;
    private String PTR;
    private String BS;

    public String getEUR() {
        return EUR;
    }

    public void setEUR(String EUR) {
        this.EUR = EUR;
    }

    public String getBTC() {
        return BTC;
    }

    public void setBTC(String BTC) {
        this.BTC = BTC;
    }

    public String getETH() {
        return ETH;
    }

    public void setETH(String ETH) {
        this.ETH = ETH;
    }

    public String getUSD() {
        return USD;
    }

    public void setUSD(String USD) {
        this.USD = USD;
    }

    public String getPTR() {
        return PTR;
    }

    public void setPTR(String PTR) {
        this.PTR = PTR;
    }

    public String getBS() {
        return BS;
    }

    public void setBS(String BS) {
        this.BS = BS;
    }
}
